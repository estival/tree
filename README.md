# tree

### 介绍

学习目录结构列出命令tree

项目源码来自于Steve Baker，其主页：

http://mama.indstate.edu/users/ice/tree/index.html


### 说明

我记得以前的windows是不带tree命令的。我也没找到适合win的tree命令

但是目前的win10带有tree，可以在cmd或者powershell中使用tree列出目录的组织结构。

今天在搜索时偶然点击关键字tree，发现linux中tree命令作者Steve Baker所发布的tree1.5.2.2版本，在gnuwin32网站中收录的有该版本的windows安装包。

为什么我以前没有搜索到。。还有这个网站是干嘛的，有人能详细科谱一下不？

网址：http://gnuwin32.sourceforge.net/packages/tree.htm

其实我对tree命令的实现不太关注，我只是想知道tree的实现中，那些网络线条是怎么跟随目录结构画出来的。

因此才有了这个项目，一方面通过tree的实现代码温习一下C，另一方面想找出如何画出那些线条。


### 附注：

win10下tree命令调出其帮助的方法如下：
```
# code block
tree /?
```
为什么ipconfig调出帮助时就不用加斜杠。。


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)